package nugroho.dimas.mvvmfirebase.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import nugroho.dimas.mvvmfirebase.data_item.isi_row
import nugroho.dimas.mvvmfirebase.viewModel.Network.repo

class MainViewModel : ViewModel() {
    private  val repo = repo()
    fun fetcUserData() : LiveData<MutableList<isi_row>>{
        val mutableData = MutableLiveData<MutableList<isi_row>>()
        repo.getData().observeForever { databar ->
         mutableData.value = databar
        }
        return  mutableData
    }
}